# How To Install and How To Run Application

## Requirements
- NodeJS ([Here](https://nodejs.org/en/download/))
- React Native ([How To Setup React Native](https://reactnative.dev/docs/environment-setup))
- XCode (If Want To Run App on IOS)
- Yarn (Optional [How To Install Yarn](https://classic.yarnpkg.com/lang/en/docs/install/))
- Emulator, Simulator or Device To Install App


## Running The Application
1. Clone this repo.
2. After Clone this Repo Run this on terminal 
```javascript
cd test-supermom
// Or Change directory to where you clone it.
```
3. Run on Terminal
```javascript
// if using NPM
npm install

// if using yarn
yarn install
```
4. Then Run this on terminal (**This is Mandatory if You Want To Run on IOS**)
```javascript
npx pod-install
```

5. After `npm install` and `pod install` Run your `emulator` then run this
```javascript
// on Android
npm run android
// or 
yarn android


// on IOS
npm run ios
// or 
yarn ios
//
```

6. (Optional Method for Run on IOS) On IOS you can open the Xcode, and open `supermomAppTest.xcworkspace`, then select emulator you want to use, then click `run` icon to install the app.

## Run Test
Run `npm run test` or if using yarn `yarn test`

## Videos
| IOS     | ANDROID |
| :---:   | :---:   |
| ![IOS](./gif/recording-ios.gif) | ![Android](./gif/recording-android.gif) |