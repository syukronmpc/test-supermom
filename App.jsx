/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

// eslint-disable-next-line no-unused-vars
import React from 'react';
import Toast from 'react-native-toast-message';
import MainScreen from './src/screens/MainScreen';

function App() {
  return (
    <>
      <MainScreen />
      <Toast />
    </>
  );
}

export default App;
