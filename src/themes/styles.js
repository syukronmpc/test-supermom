import { StyleSheet } from 'react-native';
import Colors from './colors';

const GlobalStyle = StyleSheet.create({
  container: {
    backgroundColor: Colors.backgroundDefault,
    width: '100%',
    height: '100%',
  },
});

export default GlobalStyle;
