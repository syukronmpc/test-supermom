const Colors = {
  backgroundDefault: '#F0F0F0',
  white: '#FFFFFF',
  black: '#000000',
  blue: '#87A2FB',
};

export default Colors;
