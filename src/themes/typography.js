import { StyleSheet } from 'react-native';

const GlobalTextStyle = StyleSheet.create({
  headline: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  regular: {
    fontSize: 12,
  },
  caption: {
    fontSize: 14,
    fontWeight: 'bold',
  },
});

export default GlobalTextStyle;
