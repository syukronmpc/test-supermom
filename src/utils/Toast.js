import Toast from 'react-native-toast-message';

export function showSuccessMessage(title, caption) {
  Toast.show({
    type: 'success',
    text1: title,
    text2: caption,
  });
}

export function showErrorMessage(title, caption) {
  Toast.show({
    type: 'error',
    text1: title,
    text2: caption,
  });
}
