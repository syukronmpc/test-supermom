import RNFS from 'react-native-fs';
import { showErrorMessage, showSuccessMessage } from './Toast';

class Word {
  constructor(words) {
    this.words = words;
  }

  capitalAll() {
    return String(this.words).toUpperCase();
  }

  capitalEvenCharacter() {
    function capitalEvenChar(word) {
      const chars = word.split('');

      const mappedChars = chars.map((char, index) => {
        const isOdd = index % 2 !== 0;
        return isOdd ? char.toUpperCase() : char.toLowerCase();
      });

      return mappedChars.join('');
    }

    const words = String(this.words).split(' ');
    const mappedWords = words.map((word) => capitalEvenChar(word));
    return mappedWords.join(' ').trim();
  }

  async exportWordsToCSV() {
    const { words } = this;
    const exportedData = words.split('');
    const date = new Date().getTime();
    const name = `supermom_words-${date.toString()}`;
    const content = exportedData.join(',').toString();

    // Write generated excel to Storage
    return RNFS.writeFile(`${RNFS.DocumentDirectoryPath}/${name}.csv`, content, 'ascii')
      .then(() => {
        showSuccessMessage(
          'File Successfully created.',
          `${RNFS.DocumentDirectoryPath}/${name}.csv`,
        );
        return `${RNFS.DocumentDirectoryPath}/${name}.csv`;
      })
      .catch((e) => {
        showErrorMessage('Failed to Create File', e.toString());
        return '';
      });
  }
}

export default Word;
