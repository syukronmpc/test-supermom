import 'react-native';
import Word from '../../utils/Word';

let capitalAll = '';
let capitalEven = '';

function createFile() {
  const wordUtil = new Word('hello world');
  capitalAll = wordUtil.capitalAll();
  capitalEven = wordUtil.capitalEvenCharacter();
}

test('Main Screen Create File CSV', async () => {
  createFile();

  expect(capitalAll).toEqual('HELLO WORLD');
  expect(capitalEven).toEqual('hElLo wOrLd');
});
