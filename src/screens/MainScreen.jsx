import React, { useState } from 'react';
import { ScrollView } from 'react-native';
import FileViewer from 'react-native-file-viewer';
import Text, { Variant } from '../components/Text';
import Container from '../components/Container';
import Padder from '../components/Padder';
import Spacer, { Orientation } from '../components/Spacer';
import Button from '../components/Button';
import Word from '../utils/Word';
import Input from '../components/Input';
import { showErrorMessage } from '../utils/Toast';

function MainScreen() {
  const [words, setWords] = useState('');
  const [capitalAll, setCapitalAll] = useState('');
  const [capitalEven, setCapitalEven] = useState('');
  const [createdFilePath, setCreatedFilePath] = useState('');

  async function onClickResult() {
    const wordUtil = new Word(words);
    setCapitalAll(wordUtil.capitalAll());
    setCapitalEven(wordUtil.capitalEvenCharacter());
    const path = await wordUtil.exportWordsToCSV();
    setCreatedFilePath(path);
  }

  function openFile() {
    FileViewer.open(createdFilePath)
      .then(() => {
        // success
      })
      .catch((e) => {
        showErrorMessage('Failed to open file', e.toString());
        // error
      });
  }

  function renderResult() {
    return (
      <>
        <Text variant={Variant.HEADLINE}>Result</Text>
        <Spacer orientation={Orientation.VERTICAL} />
        <Text variant={Variant.REGULAR}>{capitalAll}</Text>
        <Spacer orientation={Orientation.VERTICAL} />
        <Text variant={Variant.REGULAR}>{capitalEven}</Text>
        <Spacer orientation={Orientation.VERTICAL} />
      </>
    );
  }

  function renderOpenFileButton() {
    return (
      <>
        <Text variant={Variant.REGULAR}>{`File Saved at: ${createdFilePath}`}</Text>
        <Spacer orientation={Orientation.VERTICAL} />
        <Button caption="Open File" onPress={() => openFile()} />
      </>
    );
  }

  return (
    <Container>
      <ScrollView>
        <Padder>
          <Text variant={Variant.HEADLINE}>Word Capitalizer</Text>
          <Spacer orientation={Orientation.VERTICAL} />
          <Input
            testID="input-word"
            placeholder="Input Word"
            value={words}
            onChangeText={(text) => setWords(text)}
          />
          <Spacer amount={16} orientation={Orientation.VERTICAL} />
          <Button onPress={() => onClickResult()} caption="Show Result" />
          <Spacer amount={16} orientation={Orientation.VERTICAL} />
          {capitalAll && capitalEven && renderResult()}
          {createdFilePath && renderOpenFileButton()}
        </Padder>
      </ScrollView>
    </Container>
  );
}

export default MainScreen;
