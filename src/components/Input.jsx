import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TextInput } from 'react-native';
import Colors from '../themes/colors';

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderColor: Colors.blue,
  },
});

function Input({ value, onChangeText, placeholder, testID }) {
  return (
    <TextInput
      testID={testID}
      placeholder={placeholder}
      style={styles.input}
      value={value}
      onChangeText={onChangeText}
    />
  );
}

Input.defaultProps = {
  testID: 'component-input',
  value: undefined,
  placeholder: '',
  onChangeText: () => {},
};

Input.propTypes = {
  testID: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func,
};

export default React.memo(Input);
