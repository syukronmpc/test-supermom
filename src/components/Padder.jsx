import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

function Padder({ children }) {
  return (
    <View testID="component-padder" style={{ padding: 16 }}>
      {children}
    </View>
  );
}

Padder.propTypes = {
  children: PropTypes.node.isRequired,
};

export default React.memo(Padder);
