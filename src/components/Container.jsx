import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, StyleSheet } from 'react-native';
import Colors from '../themes/colors';
import GlobalStyle from '../themes/styles';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.backgroundDefault,
  },
});
function Container({ children, testId }) {
  return (
    <SafeAreaView testID={testId} style={[GlobalStyle.container, styles.container]}>
      {children}
    </SafeAreaView>
  );
}

Container.defaultProps = {
  testId: 'component-container',
};

Container.propTypes = {
  testId: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default React.memo(Container);
