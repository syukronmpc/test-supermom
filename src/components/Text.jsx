/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { Text as TextBase } from 'react-native';
import GlobalTextStyle from '../themes/typography';
import Colors from '../themes/colors';

export const Variant = {
  HEADLINE: 'headline',
  REGULAR: 'regular',
};

function Text({ children, variant, color, testID }) {
  if (variant === Variant.HEADLINE) {
    return (
      <TextBase testID={testID} style={[GlobalTextStyle.headline, { color }]}>
        {children}
      </TextBase>
    );
  }

  if (variant === Variant.REGULAR) {
    return (
      <TextBase testID={testID} style={[GlobalTextStyle.regular, { color }]}>
        {children}
      </TextBase>
    );
  }

  return null;
}

Text.propTypes = {
  testID: PropTypes.string,
  color: PropTypes.string,
  children: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(['regular', 'headline']),
};

Text.defaultProps = {
  testID: 'text-component',
  color: Colors.black,
  variant: 'regular',
};

export default React.memo(Text);
