import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import Colors from '../themes/colors';
import Text from './Text';
import GlobalTextStyle from '../themes/typography';

const styles = StyleSheet.create({
  button: {
    borderRadius: 12,
    backgroundColor: Colors.blue,
    paddingHorizontal: 16,
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function Button({ onPress, caption }) {
  return (
    <TouchableOpacity accessibilityLabel="component-button" onPress={onPress}>
      <View style={styles.button}>
        <Text style={GlobalTextStyle.caption} color={Colors.white}>
          {caption}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  caption: PropTypes.string.isRequired,
};

export default React.memo(Button);
