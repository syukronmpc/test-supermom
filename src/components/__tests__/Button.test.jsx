import React from 'react';
import 'react-native';
import { fireEvent, render } from '@testing-library/react-native';
import Button from '../Button';

test('Render Button and Test Press', () => {
  let text = '';
  const mockFn = () => {
    text = 'pressed';
  };

  const { getAllByLabelText } = render(<Button caption="Press Me" onPress={mockFn} />);

  const buttons = getAllByLabelText('component-button');

  fireEvent.press(buttons[0]);

  expect(text).toEqual('pressed');
});
