import React from 'react';
import 'react-native';
import { fireEvent, render } from '@testing-library/react-native';
import Input from '../Input';

test('Render Input and Change Text', () => {
  let currentText = '';
  const mockFn = (text) => {
    currentText = text;
  };

  const { queryByTestId } = render(<Input onChangeText={mockFn} placeholder="Input Here" />);
  const input = queryByTestId('component-input');

  fireEvent.changeText(input, 'testing');

  expect(currentText).toEqual('testing');
});
