import React from 'react';
import 'react-native';
import { render } from '@testing-library/react-native';
import Container from '../Container';
import Text from '../Text';

test('Render Container with Hello Text', () => {
  const container = (
    <Container>
      <Text>Hello</Text>
    </Container>
  );

  const { getByText } = render(container);

  expect(getByText('Hello')).toBeTruthy();
});
