import React from 'react';
import 'react-native';
import { render } from '@testing-library/react-native';
import Spacer, { Orientation } from '../Spacer';

test('Spacer Vertical Rendered', () => {
  const container = <Spacer amount={16} orientation={Orientation.VERTICAL} />;

  const { queryByTestId } = render(container);

  expect(queryByTestId('component-spacer')).toBeTruthy();
});

test('Spacer Horizontal Rendered', () => {
  const container = <Spacer amount={16} orientation={Orientation.HORIZONTAL} />;

  const { queryByTestId } = render(container);

  expect(queryByTestId('component-spacer')).toBeTruthy();
});
