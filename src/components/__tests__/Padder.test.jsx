import React from 'react';
import 'react-native';
import { render } from '@testing-library/react-native';
import Padder from '../Padder';
import Text from '../Text';

test('Render Padder with Hello Text', () => {
  const container = (
    <Padder>
      <Text>Hello</Text>
    </Padder>
  );

  const { queryByTestId } = render(container);

  expect(queryByTestId('component-padder')).toBeTruthy();
});
