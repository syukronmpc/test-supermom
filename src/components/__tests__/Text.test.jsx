import React from 'react';
import 'react-native';
import { render } from '@testing-library/react-native';
import Text, { Variant } from '../Text';

test('Text Headline Rendered', () => {
  const container = (
    <Text amount={16} variant={Variant.HEADLINE}>
      Hello Headline
    </Text>
  );

  const { queryByTestId } = render(container);

  expect(queryByTestId('text-component')).toBeTruthy();
});

test('Text Regular Rendered', () => {
  const container = (
    <Text amount={16} variant={Variant.REGULAR}>
      Hello Regular
    </Text>
  );

  const { queryByTestId } = render(container);

  expect(queryByTestId('text-component')).toBeTruthy();
});
