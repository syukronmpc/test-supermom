import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

export const Orientation = {
  VERTICAL: 'vertical',
  HORIZONTAL: 'horizontal',
};

function Spacer({ orientation, amount }) {
  if (orientation === Orientation.VERTICAL) {
    return <View testID="component-spacer" style={{ height: amount }} />;
  }

  if (orientation === Orientation.HORIZONTAL) {
    return <View testID="component-spacer" style={{ width: amount }} />;
  }

  return null;
}

Spacer.defaultProps = {
  orientation: Orientation.VERTICAL,
  amount: 8,
};

Spacer.propTypes = {
  amount: PropTypes.number,
  orientation: PropTypes.oneOf([Orientation.HORIZONTAL, Orientation.VERTICAL]),
};

export default React.memo(Spacer);
